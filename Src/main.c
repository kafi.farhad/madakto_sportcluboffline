/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "eeprom.h"
#include "stdio.h" 
#include "string.h"
#include "stdbool.h"
#include "stdlib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define RFID_SOF									0x02
#define RFID_EOF									0x03
#define RFID_LF										0x0A
#define RFID_CR										0x0D
#define RFID_PacketLen						12

#define bufferSize 	6
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
/* Virtual address defined by the user: 0xFFFF value is prohibitedf */
uint16_t VirtAddVarTab[NB_OF_VAR] = {0xf, 0x1f, 0xff};
RTC_AlarmTypeDef RTC_AlarmStructure;
RTC_TimeTypeDef RTC_TimeStructure;
RTC_DateTypeDef RTC_DateStructure;
__IO ITStatus UartReady = SET;
static uint8_t recentChar;	
static uint8_t cardID[6];

char rxCounter=0;
char dynamicPacketLen=0;
uint8_t *recvBuff;
bool ValidSerialPacket=false;
bool frameStarted=false;
bool fillPacket=false;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM3_Init(void);
static void MX_RTC_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
static void checkPacketValidity();		
static void manage();	
/* Private function prototypes -----------------------------------------------*/

//
uint16_t MasterCard1[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t MasterCard2[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t MasterCard3[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t MasterCard4[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t MasterCard5[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t AddressMasterCard1[bufferSize] = {0x5555, 0x5556, 0x5557,0x5558,0x5559,0x556A};
uint16_t AddressMasterCard2[bufferSize] = {0x5565, 0x5566, 0x5567,0x5568,0x5569,0x556A};
uint16_t AddressMasterCard3[bufferSize] = {0x5575, 0x5576, 0x5577,0x5578,0x5579,0x557A};
uint16_t AddressMasterCard4[bufferSize] = {0x5585, 0x5586, 0x5587,0x5588,0x5589,0x558A};
uint16_t AddressMasterCard5[bufferSize] = {0x5595, 0x5596, 0x5597,0x5598,0x5599,0x559A};

uint16_t KeyCard1[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t KeyCard2[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t KeyCard3[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t KeyCard4[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t KeyCard5[bufferSize] ={0x0000, 0x0000, 0x0000 ,0x0000 ,0x0000 ,0x0000};
uint16_t AddressKeyCard1[bufferSize] = {0x5600, 0x5601, 0x5602,0x5603,0x5604,0x5605};
uint16_t AddressKeyCard2[bufferSize] = {0x5600, 0x5601, 0x5602,0x5603,0x5604,0x5605};
uint16_t AddressKeyCard3[bufferSize] = {0x5600, 0x5601, 0x5602,0x5603,0x5604,0x5605};
uint16_t AddressKeyCard4[bufferSize] = {0x5600, 0x5601, 0x5602,0x5603,0x5604,0x5605};
uint16_t AddressKeyCard5[bufferSize] = {0x5600, 0x5601, 0x5602,0x5603,0x5604,0x5605};
char BufferInputKey[bufferSize] = {0xf, 0x1f, 0xff,0xFF,0xFF,0xF};
//
char BufferUsartRfidInput[bufferSize] = {0xf, 0x1f, 0xff,0xFF,0xFF,0xF};
char SavedBufferRfid3[bufferSize]={0x05,0x00,0x5A,0xEA,0xE3,0xA6};
uint16_t FlagSaveMasterCard[1]={0}; int Num_Of_Master_Card=0;
char BufferRfid[bufferSize]={0x05,0x00,0x5A,0xEA,0xE3,0xA6};
char SavedBufferRfid1[bufferSize]={0x05,0x00,0x5A,0xEA,0xE3,0xA6};
char SavedBufferRfid2[bufferSize]={0x05,0x00,0x5A,0xEA,0xE3,0xA6};
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
 
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
__HAL_RCC_PWR_CLK_ENABLE();
	 /* Check and Clear the Wakeup flag */
  if (__HAL_PWR_GET_FLAG(PWR_FLAG_WU) != RESET)
  {
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
  }
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_RTC_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  /*##-2- Put UART peripheral in reception process ###########################*/  
	uint8_t data_1[]="MADAKTO SPORT CLUB OFFLINE\r\n";
	UartReady = RESET;	
	HAL_UART_Transmit_DMA(&huart1, data_1, strlen((const char*)data_1));
	
  if(HAL_UART_Receive_DMA(&huart1,&recentChar, 1) != HAL_OK)
  {
    Error_Handler();
  }	
	
	/* Unlock the Flash Program Erase controller */
//  HAL_FLASH_Unlock();
//	uint8_t data_0[1];
//	
//  /* EEPROM Init */
//	EE_Init();	
//	/* Initialize all configured peripherals */
////	configAlarm();
////   HAL_TIM_Base_Start_IT(&htim3);
//	 if (BufferRfid[0]==SavedBufferRfid1[0] )
//		{
//			if (BufferRfid[1]==SavedBufferRfid1[1])
//			{
//				if (BufferRfid[2]==SavedBufferRfid1[2])
//				{
//					if (BufferRfid[3]==SavedBufferRfid1[3])
//					{
//						if (BufferRfid[4]==SavedBufferRfid1[4])
//						{
//							HAL_UART_Transmit(&huart1,BufferRfid,6,10);
//						}
//					}
//				}			
//			}			
//		}

//		HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_7, GPIO_PIN_SET);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
//	 EE_ReadVariable(AddressMasterCard1[0], &MasterCard1[0]) ;
//	 if ( MasterCard1[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressMasterCard1[1], &MasterCard1[1]);
//		 EE_ReadVariable(AddressMasterCard1[2], &MasterCard1[2]);
//		 EE_ReadVariable(AddressMasterCard1[3], &MasterCard1[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressMasterCard2[0], &MasterCard2[0]);
//	 if ( MasterCard2[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressMasterCard2[1], &MasterCard2[1]);
//		 EE_ReadVariable(AddressMasterCard2[2], &MasterCard2[2]);
//		 EE_ReadVariable(AddressMasterCard2[3], &MasterCard2[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressMasterCard3[0], &MasterCard3[0]);
//	 if ( MasterCard3[0] != 0xFF )
//	 {		 
//		 EE_ReadVariable(AddressMasterCard3[1], &MasterCard3[1]);
//		 EE_ReadVariable(AddressMasterCard3[2], &MasterCard3[2]);
//		 EE_ReadVariable(AddressMasterCard3[3], &MasterCard3[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressMasterCard4[0], &MasterCard4[0]);
//	 if ( MasterCard4[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressMasterCard4[1], &MasterCard4[1]);
//		 EE_ReadVariable(AddressMasterCard4[2], &MasterCard4[2]);
//		 EE_ReadVariable(AddressMasterCard4[3], &MasterCard4[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressMasterCard5[0], &MasterCard5[0]);
//	 if ( MasterCard5[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressMasterCard5[1], &MasterCard5[1]);
//		 EE_ReadVariable(AddressMasterCard5[2], &MasterCard5[2]);
//		 EE_ReadVariable(AddressMasterCard5[3], &MasterCard5[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressKeyCard1[0], &KeyCard1[0]);
//	 if ( KeyCard1[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressKeyCard1[1], &KeyCard1[1]);
//		 EE_ReadVariable(AddressKeyCard1[2], &KeyCard1[2]);
//		 EE_ReadVariable(AddressKeyCard1[3], &KeyCard1[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressKeyCard2[0], &KeyCard2[0]);
//	 if ( KeyCard1[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressKeyCard2[1], &KeyCard2[1]);
//		 EE_ReadVariable(AddressKeyCard2[2], &KeyCard2[2]);
//		 EE_ReadVariable(AddressKeyCard2[3], &KeyCard2[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressKeyCard3[0], &KeyCard3[0]);
//	 if ( KeyCard1[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressKeyCard3[1], &KeyCard3[1]);
//		 EE_ReadVariable(AddressKeyCard3[2], &KeyCard3[2]);
//		 EE_ReadVariable(AddressKeyCard3[3], &KeyCard3[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressKeyCard4[0], &KeyCard4[0]);
//	 if ( KeyCard1[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressKeyCard4[1], &KeyCard4[1]);
//		 EE_ReadVariable(AddressKeyCard4[2], &KeyCard4[2]);
//		 EE_ReadVariable(AddressKeyCard4[3], &KeyCard4[3]);
//	 }
//	 
//	 EE_ReadVariable(AddressKeyCard5[0], &KeyCard5[0]);
//	 if ( KeyCard1[0] != 0xFF)
//	 {
//		 EE_ReadVariable(AddressKeyCard5[1], &KeyCard5[1]);
//		 EE_ReadVariable(AddressKeyCard5[2], &KeyCard5[2]);
//		 EE_ReadVariable(AddressKeyCard5[3], &KeyCard5[3]);
//	 }
  while (1)
  {
//    HAL_Delay(1000);
//		HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_3, GPIO_PIN_RESET);
//		HAL_Delay(1000);
//		HAL_GPIO_WritePin(GPIOA,  GPIO_PIN_3, GPIO_PIN_SET);
//		// get ID Card
//		if (BufferInputKey[0]==SavedBufferRfid1[0] )
//		{
//			if (BufferInputKey[1]==SavedBufferRfid1[1])
//			{
//				if (BufferInputKey[2]==SavedBufferRfid1[2])
//				{
//					if (BufferInputKey[3]==SavedBufferRfid1[3])
//					{
//						if (BufferInputKey[4]==SavedBufferRfid1[4])
//						{
//							HAL_UART_Transmit(&huart1,BufferRfid,6,10);
//						}
//					}
//				}			
//			}			
//		}
//		// define the Master Card
//		EE_WriteVariable( AddressMasterCard1[0], BufferInputKey[0] );
//		while (FlagSaveMasterCard[0]==1)
//		{			
//			if (Num_Of_Master_Card==0)
//			{
//				EE_WriteVariable( AddressMasterCard1[0], BufferInputKey[0] );
//				EE_WriteVariable( AddressMasterCard1[1], BufferInputKey[1] );
//				EE_WriteVariable( AddressMasterCard1[2], BufferInputKey[2] );
//				EE_WriteVariable( AddressMasterCard1[3], BufferInputKey[3] );
//				EE_WriteVariable( AddressMasterCard1[4], BufferInputKey[4] );				
//				Num_Of_Master_Card++;
//			}				
//			if (Num_Of_Master_Card==1)
//			{
//				EE_WriteVariable( AddressMasterCard2[0], BufferInputKey[0] );
//				EE_WriteVariable( AddressMasterCard2[1], BufferInputKey[1] );
//				EE_WriteVariable( AddressMasterCard2[2], BufferInputKey[2] );
//				EE_WriteVariable( AddressMasterCard2[3], BufferInputKey[3] );
//				EE_WriteVariable( AddressMasterCard2[4], BufferInputKey[4] );
//				Num_Of_Master_Card++;
//			}			
//			if (Num_Of_Master_Card==2)
//			{
//				EE_WriteVariable( AddressMasterCard3[0], BufferInputKey[0] );
//				EE_WriteVariable( AddressMasterCard3[1], BufferInputKey[1] );
//				EE_WriteVariable( AddressMasterCard3[2], BufferInputKey[2] );
//				EE_WriteVariable( AddressMasterCard3[3], BufferInputKey[3] );
//				EE_WriteVariable( AddressMasterCard3[4], BufferInputKey[4] );
//				Num_Of_Master_Card++;
//			}			
//			if (Num_Of_Master_Card==3)
//			{
//				EE_WriteVariable( AddressMasterCard4[0], BufferInputKey[0] );
//				EE_WriteVariable( AddressMasterCard4[1], BufferInputKey[1] );
//				EE_WriteVariable( AddressMasterCard4[2], BufferInputKey[2] );
//				EE_WriteVariable( AddressMasterCard4[3], BufferInputKey[3] );
//				EE_WriteVariable( AddressMasterCard4[4], BufferInputKey[4] );
//				Num_Of_Master_Card++;
//			}			
//			if (Num_Of_Master_Card==4)
//			{
//				EE_WriteVariable( AddressMasterCard5[0], BufferInputKey[0] );
//				EE_WriteVariable( AddressMasterCard5[1], BufferInputKey[1] );
//				EE_WriteVariable( AddressMasterCard5[2], BufferInputKey[2] );
//				EE_WriteVariable( AddressMasterCard5[3], BufferInputKey[3] );
//				EE_WriteVariable( AddressMasterCard5[4], BufferInputKey[4] );
//				Num_Of_Master_Card++;
//			}			
//		}
		// define the KEY
		// Routin Of Program 
	}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_RTC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI0_1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
  /* EXTI2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);
  /* TIM1_BRK_UP_TRG_COM_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_BRK_UP_TRG_COM_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM1_BRK_UP_TRG_COM_IRQn);
  /* TIM3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM3_IRQn);
  /* USART1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* RTC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(RTC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(RTC_IRQn);
}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /**Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
    
  /* USER CODE END Check_RTC_BKUP */

  /**Initialize RTC and set the Time and Date 
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /**Enable the Alarm A 
  */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 10;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 10000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 0;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel4_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_3, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA0 PA3 PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_3|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* RTC init function */

 void HAL_GPIO_EXTI_Callback(uint16_t GPIO_PIN)
{
//		
//		if ( FLAG_INT_GLOBAL1==1&& FLAG_INT_GLOBAL2==0  )
//		{
//			HAL_TIM_Base_Start_IT(&htim3);
//		  Num_Of_Master_Card =0;
//			FlagSaveMasterCard[0]=1;//start, For define: master Card
//			uint8_t data_1[]="INT1\r\n";
//			HAL_UART_Transmit(&huart1,data_1,6,10);
//		}
//		if (FLAG_INT_GLOBAL1==0 &&FLAG_INT_GLOBAL2==1 )
//		{
//				uint8_t data_2[]="INT2\r\n";
//				HAL_UART_Transmit(&huart1,data_2,6,10);
//		}
	
}
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	
//	uint8_t data_3[]="TIMCALLBACK\r\n";
//	HAL_UART_Transmit(&huart1,data_3,11,10);
//	HAL_GPIO_TogglePin( GPIOA,  GPIO_PIN_0 );
//	FlagSaveMasterCard[0]=0;//Stop , For define: master Card
//	HAL_TIM_Base_Stop_IT(&htim3);
}
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
//	uint8_t data_1[]="INT\r\n";
//	HAL_UART_Transmit(&huart1,data_1,6,10);	
}

static void configAlarm(void)
{
    HAL_RTC_GetTime(&hrtc, &RTC_TimeStructure, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&hrtc, &RTC_DateStructure, RTC_FORMAT_BIN);

    /* Set the alarm to current time + 5s */
    RTC_AlarmStructure.Alarm  = RTC_ALARM_A;
    RTC_AlarmStructure.AlarmTime.TimeFormat = RTC_TimeStructure.TimeFormat;
    RTC_AlarmStructure.AlarmTime.Hours = RTC_TimeStructure.Hours;
    RTC_AlarmStructure.AlarmTime.Minutes = RTC_TimeStructure.Minutes;
    RTC_AlarmStructure.AlarmTime.Seconds = (RTC_TimeStructure.Seconds + 5);
		RTC_AlarmStructure.AlarmTime.SubSeconds = 304;
    RTC_AlarmStructure.AlarmDateWeekDay = 31;
    RTC_AlarmStructure.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
    RTC_AlarmStructure.AlarmMask = RTC_ALARMMASK_ALL;
    RTC_AlarmStructure.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_SS14_7;
    if (HAL_RTC_SetAlarm_IT(&hrtc, &RTC_AlarmStructure, RTC_FORMAT_BIN) != HAL_OK)
    {
      /* Initialization Error */
      Error_Handler();
    }
  
    /* Request to enter STANDBY mode */
    HAL_PWR_EnterSTANDBYMode();		
}

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of DMA Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	UartReady = SET;
}
/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{

			//second	
		if(frameStarted) 
		{						
			frameStarted = false;
			fillPacket = true;
			rxCounter=0;
			dynamicPacketLen = RFID_PacketLen;					
			recvBuff = malloc(dynamicPacketLen);						
		}
		
		//first
		if(recentChar == RFID_SOF && !fillPacket) 				
			frameStarted = true;// dont save SOF											
		
		//third
		if(fillPacket && (rxCounter < dynamicPacketLen))			
			recvBuff[rxCounter++] = recentChar;													
		
		else if(fillPacket && (rxCounter >= dynamicPacketLen))		
		{				
			recvBuff[rxCounter] = recentChar;//without EOF.
			dynamicPacketLen=0;
			rxCounter=0;
			fillPacket = false;
			checkPacketValidity();							
		}
  /*##-2- Put UART peripheral in reception process ###########################*/  
  if(HAL_UART_Receive_DMA(&huart1,&recentChar, 1) != HAL_OK)
  {
    Error_Handler();
  }		
}
//
void checkPacketValidity(void)
{			  			
	if(recvBuff[RFID_PacketLen-1] == RFID_LF  &&
		 recvBuff[RFID_PacketLen-2] == RFID_CR   )
	{			
				manage();										
	}				
	free(recvBuff);
}
//
void manage(void)
{			
	UartReady = RESET;
	for(int i =4; i<RFID_PacketLen-2;i++)
		cardID[i-4]=recvBuff[i];
	
	HAL_UART_Transmit_DMA(&huart1,cardID,6);
	
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
		uint8_t data_1[]="ERR2\n\r";
		UartReady = RESET;	
		HAL_UART_Transmit_DMA(&huart1, data_1, 6);
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
